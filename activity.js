// 1. What directive is used by Node.js in loading the modules it needs?
    /* ans.

            - require
    */


// 2. What Node.js module contains a method for server creation?
    /* ans.

            - http module
    */


// 3. What is the method of the http object responsible for creating a server using Node.js?
    /* ans.

            - createServer()
    */



// 4. What method of the response object allows us to set status codes and content types?
    /* ans.

            - writeHead
    */


// 5. Where will console.log() output its contents when run in Node.js?
    /* ans.

            - terminal
    */


// 6. What property of the request object contains the address's endpoint?
    /* ans.

            - req.url
    */
