const http = require("http");
const port = 3000


const server = http.createServer((request, response) => 
	{
		if(request.url == '/homepage'){
			response.writeHead(200, {'Content-Type':'text/plain'})
			response.end('Server now successfully running.')

		}else if(request.url == '/login'){
			response.writeHead(200,{'Content-Type': 'text/plain'})
			response.end("Welcome to Log-in page.")

		}else{
			response.writeHead(400,{'Content-Type': 'text/plain'})
			response.end("I'm sorry the page you are looking for cannot be found.")
		}
	}
)


server.listen(port)
console.log(`Server now accessible at localhosts: ${port}`)



